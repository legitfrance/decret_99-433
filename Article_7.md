Deux personnes qui exercent dans la même entreprise ne peuvent siéger simultanément dans un même établissement ou délégation du réseau des chambres de métiers et de l'artisanat.

Lorsque deux personnes qui exercent dans la même entreprise ont été élues, la moins âgée peut seule être proclamée élue. Le siège ainsi laissé libre par l'autre est attribué au suivant de liste.
