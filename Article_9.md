I. - La liste des électeurs est établie à l'occasion de chaque renouvellement quinquennal.

II. - Si les circonstances l'exigent, le préfet compétent prescrit la révision de la liste des électeurs concernée, par arrêté publié au Journal officiel de la République française.

L'arrêté préfectoral fixe la date des différentes opérations que comporte cette révision.

Le préfet compétent est :

1° Pour les chambres de métiers et de l'artisanat départementales : le préfet de département ;

2° Pour les chambres de métiers et de l'artisanat interdépartementales : le préfet du lieu du siège de la chambre de métiers et de l'artisanat interdépartementale ;

3° Pour les chambres de métiers et de l'artisanat de région : le préfet de région.
