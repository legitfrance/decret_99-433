Au plus tard le premier jour du mois précédant celui de la date de clôture du scrutin, le préfet compétent arrête la liste des électeurs, après avoir vérifié qu'il a été procédé à toutes les rectifications ordonnées.

Lorsque cette date est un samedi, un dimanche, un jour férié ou chômé, la liste des électeurs est arrêtée le jour ouvrable précédent.
