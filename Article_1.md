I.-Le nombre de membres élus dans chaque chambre régionale de métiers et de l'artisanat est fixé comme suit :

<table><tbody><tr><th>
NOMBRE DE DÉPARTEMENTS

</th><th>
NOMBRE D'ÉLUS

par délégation départementale

</th><th>
NOMBRE D'ÉLUS

par département

siégeant à la chambre régionale

de métiers et de l'artisanat

</th><th>
NOMBRE TOTAL D'ÉLUS

dans le ressort de la chambre régionale

de métiers et de l'artisanat

</th></tr><tr><td align="center" valign="middle">
2
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
50
</td></tr><tr><td align="center" valign="middle">
3
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
75
</td></tr><tr><td align="center" valign="middle">
4
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
100
</td></tr><tr><td align="center" valign="middle">
5
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
20
</td><td align="center" valign="middle">
100
</td></tr><tr><td align="center" valign="middle">
6
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
16
</td><td align="center" valign="middle">
96
</td></tr><tr><td align="center" valign="middle">
7
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
14
</td><td align="center" valign="middle">
98
</td></tr><tr><td align="center" valign="middle">
8
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
12
</td><td align="center" valign="middle">
96
</td></tr><tr><td align="center" valign="middle">
9
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
11
</td><td align="center" valign="middle">
99
</td></tr><tr><td align="center" valign="middle">
10
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
10
</td><td align="center" valign="middle">
100
</td></tr><tr><td align="center" valign="middle">
11
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
9
</td><td align="center" valign="middle">
99
</td></tr><tr><td align="center" valign="middle">
12
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
8
</td><td align="center" valign="middle">
96
</td></tr><tr><td align="center" valign="middle">
13
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
7
</td><td align="center" valign="middle">
91
</td></tr></tbody></table>

II.-Les membres de la chambre régionale de métiers et de l'artisanat, élus dans le département, siègent à l'assemblée générale de la chambre régionale de métiers et de l'artisanat et à celle de la chambre de métiers et de l'artisanat départementale ou interdépartementale.

III.-Chaque chambre de métiers et de l'artisanat départementale est composée de vingt-cinq membres élus dans les conditions prévues à l'article 3.

Chaque chambre de métiers et de l'artisanat départementale comprend :

1° Les membres de la chambre régionale des métiers et de l'artisanat élus dans le département ;

2° Les membres de la chambre de métiers et de l'artisanat départementale élus dans ce département.
