La commission d'organisation des élections se réunit sur convocation de son président. Elle est chargée :

1° D'expédier aux électeurs les circulaires et les bulletins de vote ainsi que les instruments nécessaires au vote par correspondance ;

2° D'organiser la réception des votes ;

3° D'organiser le dépouillement et le recensement des votes ;

4° De proclamer la liste des candidats élus en qualité de membres des établissements du réseau des chambres de métiers et de l'artisanat et de leurs délégations ;

5° De statuer sur les demandes de remboursement des frais de propagande des candidats.

Pour assurer ces opérations, le président de la commission peut solliciter le concours de la délégation ou de la chambre de métiers et de l'artisanat départementale ainsi que de la chambre de métiers et de l'artisanat de région ou de la chambre régionale de métiers et de l'artisanat.
