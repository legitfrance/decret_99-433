Sous réserve des dispositions de l'article 33, le ministre chargé de l'artisanat convoque les électeurs et arrête la date d'ouverture de la campagne électorale, au plus tard le premier jour du mois précédant celui de la date de clôture du scrutin. Lorsque cette date est un samedi, un dimanche, un jour férié ou chômé, cette date d'ouverture est arrêtée le jour ouvrable précédent

La campagne électorale débute le quatorzième jour précédant le dernier jour du scrutin et s'achève la veille de celui-ci, à minuit.
