I. - Nul ne peut être candidat sur plus d'une liste ni dans plus d'un département d'une même région.

Les candidatures qui ne se conforment pas à cette règle sont irrecevables. En cas de candidatures multiples, seule la première des candidatures déposées est recevable.

II. - La déclaration de candidature résulte du dépôt à la préfecture d'une liste répondant aux conditions fixées par le présent décret.

La liste déposée comporte expressément :

1° Le titre de la liste présentée et le nom du responsable de la liste ;

2° Les noms de famille et, le cas échéant, d'épouse, les prénoms, le sexe, la date et le lieu de naissance, la profession, la catégorie d'activité, le numéro d'immatriculation au répertoire des métiers et l'adresse du siège de l'entreprise de chacun des candidats tel qu'il figure au répertoire des métiers ;

3° L'attestation délivrée par la chambre de métiers et de l'artisanat départementale ou la chambre de métiers et de l'artisanat interdépartementale ou la chambre de métiers et de l'artisanat de région des personnes inscrites dans la section des métiers d'art du répertoire des métiers.

La liste des candidats est accompagnée de l'ensemble des déclarations individuelles de candidatures signées des candidats.

Chaque candidat doit également produire une attestation de la chambre de métiers et de l'artisanat de région ou de la chambre de métiers et de l'artisanat départementale ou interdépartementale constatant qu'il remplit les conditions fixées aux II et III de l'article 6. Cette opération peut être accomplie par un mandataire, ayant qualité d'électeur, pour le compte de chaque candidat.
