La liste des électeurs est établie, par département, par la chambre de métiers et de l'artisanat de région ou par la chambre de métiers et de l'artisanat départementale ou interdépartementale le dernier jour du sixième mois précédant celui de la date de clôture du scrutin en vue du renouvellement quinquennal ou à une date fixée par l'arrêté préfectoral mentionné au II de l'article 9. Lorsque cette date est un samedi, un dimanche, un jour férié ou chômé, la liste des électeurs est arrêtée le jour ouvrable précédent. Le président de la chambre de métiers et de l'artisanat de région ou de la chambre de métiers et de l'artisanat départementale ou interdépartementale transmet au préfet compétent un exemplaire signé de la liste des électeurs, ainsi que le compte rendu constatant l'accomplissement des opérations de révision de cette liste, dans les cinq jours au plus tard qui suivent l'établissement de celle-ci.

Si le préfet compétent estime que les formalités et les délais prescrits n'ont pas été observés, il doit, dans les deux jours suivant la réception de la liste, déférer cette dernière au tribunal administratif, qui statue dans les trois jours et fixe éventuellement le délai dans lequel il devra être procédé à de nouvelles opérations.

Cette liste est établie dans l'ordre alphabétique du nom de famille des électeurs.

Doivent figurer sur la liste le nom de famille, le nom d'épouse, les prénoms, le sexe, la date et le lieu de naissance, le domicile, la profession de l'électeur et sa catégorie d'activité, complétés pour les électeurs concernés de la mention de leur inscription à la section des métiers d'art du répertoire des métiers ainsi qu'en outre :

1° Pour les personnes physiques immatriculées au répertoire des métiers, l'adresse de l'entreprise ou de son établissement principal et leur numéro d'immatriculation au répertoire des métiers ;

2° Pour les conjoints collaborateurs, l'adresse de l'entreprise ou de son établissement principal et le numéro d'immatriculation au répertoire des métiers sous lequel ils sont mentionnés ;

3° Pour les dirigeants sociaux, l'adresse du siège de l'entreprise et le numéro d'immatriculation au répertoire des métiers de la personne morale.
