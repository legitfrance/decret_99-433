Dans les cinq jours qui suivent la date de réception de la liste des électeurs, le préfet compétent informe les électeurs du dépôt de celle-ci et de la possibilité de la consulter pendant une durée de dix jours, par voie d'affiches apposées à la préfecture, au siège des établissements du réseau des chambres de métiers et de l'artisanat et de leurs délégations et, le cas échéant, par tout autre moyen à sa convenance.

Lorsque la consultation des listes électorales par voie électronique est prévue, elle doit s'effectuer dans des conditions de sécurité et de confidentialité assurant le respect du code électoral.

Tout électeur est autorisé à prendre communication de la liste des électeurs et à en obtenir copie à ses frais auprès de la chambre de métiers et de l'artisanat de région ou de la chambre de métiers et de l'artisanat départementale ou interdépartementale.

Tout usage commercial de la liste des électeurs établies pour les élections aux délégations et établissements composant le réseau des chambres de métiers et de l'artisanat est puni de l'amende prévue pour les contraventions de la 5e classe.
