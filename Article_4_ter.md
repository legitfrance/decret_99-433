I. - Chaque délégation départementale de chambre de métiers et de l'artisanat interdépartementale est composée de vingt-cinq membres élus dans les conditions suivantes :

Chaque chambre de métiers et de l'artisanat interdépartementale comprend :

1° Les membres de la chambre de métiers et de l'artisanat interdépartementale siégeant à la chambre régionale des métiers et de l'artisanat. Le nombre d'élus interdépartementaux est égal au nombre d'élus régionaux par département mentionné dans la colonne 3 du tableau ci-dessous multiplié par le nombre de délégations départementales de la chambre de métiers et de l'artisanat interdépartementale ;

2° Les membres de la chambre de métiers et de l'artisanat interdépartementale élus dans chaque délégation départementale.

Le nombre de membres élus dans chaque chambre de métiers et de l'artisanat interdépartementale est fixé comme suit :

<table><tbody><tr><th>
NOMBRE DE DÉPARTEMENTS

</th><th>
NOMBRE D'ÉLUS

par délégation départementale

</th><th>
NOMBRE D'ÉLUS

par département

siégeant à la chambre de métiers

et de l'artisanat interdépartementale

</th><th>
NOMBRE TOTAL D'ÉLUS

dans le ressort de la chambre de métiers

et de l'artisanat interdépartementale

</th></tr><tr><td align="center" valign="middle">
2
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
50
</td></tr><tr><td align="center" valign="middle">
3
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
75
</td></tr><tr><td align="center" valign="middle">
4
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
100
</td></tr><tr><td align="center" valign="middle">
5
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
20
</td><td align="center" valign="middle">
100
</td></tr><tr><td align="center" valign="middle">
6
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
16
</td><td align="center" valign="middle">
96
</td></tr><tr><td align="center" valign="middle">
7
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
14
</td><td align="center" valign="middle">
98
</td></tr><tr><td align="center" valign="middle">
8
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
12
</td><td align="center" valign="middle">
96
</td></tr><tr><td align="center" valign="middle">
9
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
11
</td><td align="center" valign="middle">
99
</td></tr><tr><td align="center" valign="middle">
10
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
10
</td><td align="center" valign="middle">
100
</td></tr><tr><td align="center" valign="middle">
11
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
9
</td><td align="center" valign="middle">
99
</td></tr><tr><td align="center" valign="middle">
12
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
8
</td><td align="center" valign="middle">
96
</td></tr><tr><td align="center" valign="middle">
13
</td><td align="center" valign="middle">
25
</td><td align="center" valign="middle">
7
</td><td align="center" valign="middle">
91
</td></tr></tbody></table>

II. - Les membres des chambres de métiers et de l'artisanat interdépartementales sont élus en même temps, au scrutin de liste interdépartemental à un tour, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation, par l'ensemble des électeurs des départements composant la chambre de métiers et de l'artisanat interdépartementale.

Pour être complète, chaque section départementale de liste interdépartementale doit comprendre un nombre de candidats au moins égal à trente-cinq.

Chaque liste comporte au moins quatre candidats pour chacune des catégories qui regroupent les activités figurant en annexe du décret du 2 avril 1998 susvisé parmi les dix-huit premiers candidats de chacune des listes.

Au moins un candidat inscrit dans la section métiers d'art du répertoire des métiers figure parmi les sept premiers candidats de chacune des listes.

Chaque liste est composée d'au moins un candidat de chaque sexe par groupe de trois candidats.

III. - Pour la répartition des sièges des membres de la chambre de métiers et de l'artisanat interdépartementale siégeant à la chambre régionale de métiers et de l'artisanat, il est attribué, par département, à la liste qui a recueilli le plus de voix au niveau interdépartemental, un nombre de sièges égal à 50 % du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur. En cas d'égalité de suffrages entre les listes arrivées en tête, ces sièges sont attribués à la liste dont les candidats ont la moyenne d'âge la moins élevée.

Cette attribution opérée, les autres sièges à pourvoir, dans le département, sont répartis en fonction des suffrages exprimés dans le département entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne.

Les sièges sont attribués aux candidats dans l'ordre de présentation sur chaque liste.

Si plusieurs listes ont la même moyenne pour l'attribution du dernier siège, celui-ci revient à la liste qui a obtenu le plus grand nombre de suffrages. En cas d'égalité de suffrages, le siège est attribué au moins âgé des candidats susceptibles d'être proclamés élus.

IV. - Une fois effectuée l'attribution des sièges des membres de la chambre de métiers et de l'artisanat interdépartementale siégeant à la chambre régionale de métiers et de l'artisanat en application du III, les sièges des membres des délégations départementales restant à attribuer sont répartis dans les mêmes conditions entre les listes, un nombre de sièges égal à 30 % du nombre de sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur, étant attribué à la liste qui a recueilli le plus de voix. Pour chacune de ces listes, les sièges sont attribués dans l'ordre de présentation en commençant par le premier des candidats non proclamés élus de la chambre régionale de métiers et de l'artisanat.

V. - Les listes qui n'ont pas obtenu au moins 5 % des suffrages exprimés ne sont pas admises à la répartition des sièges.

VI. - Les dispositions des articles 3-1 et 4 s'appliquent aux élections aux chambres de métiers et de l'artisanat interdépartementales.
