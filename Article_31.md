Le président de la commission proclame en public la liste des candidats relevant de la circonscription de la délégation ou de la chambre de métiers et de l'artisanat départementale élus à la chambre de métiers et de l'artisanat de région ou à la chambre régionale de métiers et de l'artisanat ou à la chambre de métiers et de l'artisanat interdépartementale et la liste des candidats élus à la section ou à la chambre de métiers et de l'artisanat départementale.

Après proclamation des résultats, un procès-verbal est dressé par la commission et signé par le président et les membres de celle-ci.

La liste d'émargement et le procès-verbal des opérations de vote sont transmis immédiatement au préfet compétent. Ils peuvent être consultés par tout électeur pendant dix jours.

Le préfet compétent  transmet dans les trois jours une copie du procès-verbal au ministre chargé de l'artisanat, au secrétariat de la délégation ou de la chambre de métiers et de l'artisanat départementale et à celui de la chambre de métiers et de l'artisanat de région ou de la chambre régionale de métiers et de l'artisanat ou de la chambre de métiers et de l'artisanat interdépartementale.
