I.-Les membres des chambres de métiers et de l'artisanat départementales et des chambres régionales de métiers et de l'artisanat sont élus en même temps, au scrutin de liste départemental à un tour, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation, par l'ensemble des électeurs.

Pour être complète, une liste doit comprendre au moins trente-cinq candidats.

Chaque liste comporte au moins quatre candidats pour chacune des catégories qui regroupent les activités figurant en annexe du décret du 2 avril 1998 susvisé parmi les dix-huit premiers candidats de chacune des listes.

Au moins un candidat inscrit dans la section métiers d'art du répertoire des métiers figure parmi les sept premiers candidats de chacune des listes.

Chaque liste est composée d'au moins un candidat de chaque sexe par groupe de trois candidats.

II.-Pour la répartition des sièges des membres de la chambre régionale de métiers et de l'artisanat, il est attribué à la liste qui a recueilli le plus de voix un nombre de sièges égal à 50 % du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur. En cas d'égalité de suffrages entre les listes arrivées en tête, ces sièges sont attribués à la liste dont les candidats ont la moyenne d'âge la moins élevée.

Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne.

Les sièges sont attribués aux candidats dans l'ordre de présentation sur chaque liste.

Si plusieurs listes ont la même moyenne pour l'attribution du dernier siège, celui-ci revient à la liste qui a obtenu le plus grand nombre de suffrages. En cas d'égalité de suffrages, le siège est attribué au moins âgé des candidats susceptibles d'être proclamés élus.

III.-Une fois effectuée l'attribution des sièges des membres de la chambre régionale de métiers et de l'artisanat en application du II, les sièges des membres de la chambre de métiers et de l'artisanat départementale restant à attribuer sont répartis dans les mêmes conditions entre les listes, un nombre de sièges égal à 30 % du nombre de sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur, étant attribué à la liste qui a recueilli le plus de voix. Pour chacune de ces listes, les sièges sont attribués dans l'ordre de présentation en commençant par le premier des candidats non proclamés élus de la chambre régionale de métiers et de l'artisanat.

Les dispositions de l'alinéa précédent ne s'appliquent pas à la   collectivité de Corse.

IV.-Les listes qui n'ont pas obtenu au moins 5 % des suffrages exprimés ne sont pas admises à la répartition des sièges.

V.-Le présent article ne s'applique pas aux désignations des représentants des membres des chambres de métiers régies par les articles 103 et suivants du code professionnel local maintenu en vigueur par la loi du 1er juin 1924 mettant en vigueur la législation civile française dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle. Ces chambres de métiers désignent parmi leurs membres ceux d'entre eux qui siégeront à la chambre de métiers et de l'artisanat de région ou à la chambre régionale de métiers et de l'artisanat en nombre égal à celui des autres départements.
