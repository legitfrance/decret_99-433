Une commission d'organisation des élections est instituée par arrêté du préfet compétent dans chaque circonscription électorale au plus tard le premier jour du mois précédant celui de la date de clôture du scrutin. Elle est composée :

1° Pour les chambres de métiers et de l'artisanat de région : d'un représentant du préfet de région ;

2° Pour les chambres régionales de métiers et de l'artisanat : du préfet de département ou de son représentant, président, d'un représentant du préfet du lieu du siège de la chambre de métiers et de l'artisanat interdépartementale et d'un représentant du préfet de région ;

3° D'un membre de la délégation ou de la chambre de métiers et de l'artisanat départementale désigné par le président de la chambre de métiers et de l'artisanat de région ou de la chambre de métiers et de l'artisanat départementale ;

4° D'un membre de la chambre de métiers et de l'artisanat de région ou de la chambre régionale de métiers et de l'artisanat désigné par le président de cette chambre ;

5° Le cas échéant, d'un membre de la chambre de métiers et de l'artisanat interdépartementale ;

6° D'un représentant de la ou des entreprises chargées de l'acheminement des plis pour les attributions mentionnées aux 1° et 2° de l'article 26 ;

Dans le Département de Mayotte, la commission d'organisation des élections est composée :

1° Du préfet ou de son représentant, président ;

2° D'un membre de la chambre de métiers et de l'artisanat désigné par le président de cette chambre ;

3° D'un représentant de la ou des entreprises chargées de l'acheminement des plis pour les attributions mentionnées aux 1° et 2° de l'article 26.

Le secrétariat de la commission est assuré par les services de la préfecture compétente.

Les candidats ou les mandataires des listes peuvent participer, avec voie consultative, aux travaux de la commission.
